FROM debian:bullseye

RUN \
    apt-get update && \
    apt-get install --yes \
        certbot \
        python3-certbot-dns-cloudflare \
        kubernetes-client \
    && \
    apt-get clean

COPY entrypoint /root/entrypoint

ENTRYPOINT /root/entrypoint

